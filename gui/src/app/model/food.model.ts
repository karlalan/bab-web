export class Food {
	name: string;
	price: number;
	unit: string;
	currency: string;
}